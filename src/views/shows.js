import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import ShowsS1 from "../components/shows-s1";
import ShowsS2 from "../components/shows-s2";
import ShowsS3 from "../components/shows-s3";

class Shows extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <ShowsS1/>
                <ShowsS2/>
                <ShowsS3/>
            </Container>    
        );
    }
}

export default Shows;