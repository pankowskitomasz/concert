import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import ArtistsS1 from "../components/artists-s1";
import ArtistsS2 from "../components/artists-s2";

class Artists extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <ArtistsS1/>
                <ArtistsS2/>
            </Container>    
        );
    }
}

export default Artists;